//
//  ViewControllerTwo.swift
//  UserDefaults
//
//  Created by Cristhian Motoche on 30/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewControllerTwo: UIViewController, Viewcontroller3Delegate {

    @IBOutlet weak var labelInfo: UILabel!

    override func viewDidLoad() {
        UserDefaults.standard.set(true, forKey: "isLogged")

        super.viewDidLoad()
        guard let message = UserDefaults.standard.object(forKey: "message") as? String else {
            return
        }
        self.labelInfo.text = message
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillDisappear(_ animated: Bool) {
        guard let message = self.labelInfo.text else {
            return
        }
        UserDefaults.standard.set(message, forKey: "message")
    }

    @IBAction func salir(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isLogged")
        UIView.animate(withDuration: 1, animations: {
            self.view.backgroundColor = UIColor.gray
        }) { (isTrue) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func ingresoMensaje(msg: String) {
        self.labelInfo.text = msg
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dvc = segue.destination as! ViewControllerThree
        dvc.delegate = self
    }
}
