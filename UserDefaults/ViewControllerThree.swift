//
//  ViewControllerThree.swift
//  UserDefaults
//
//  Created by Cristhian Motoche on 30/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

@objc protocol Viewcontroller3Delegate:class {
    func ingresoMensaje(msg:String)
    @objc optional func funcOptional()
}

class ViewControllerThree: UIViewController {
    var delegate: Viewcontroller3Delegate!

    @IBOutlet weak var searchText: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func aceptarDelegate(_ sender: Any) {
        self.delegate.ingresoMensaje(msg: searchText.text!)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func salir(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        UserDefaults.standard.set(false, forKey: "isLogged")
    }
}
