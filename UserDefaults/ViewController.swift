//
//  ViewController.swift
//  UserDefaults
//
//  Created by Cristhian Motoche on 30/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        guard let isLogged = UserDefaults.standard.object(forKey: "isLogged") as? Bool else {
            return
        }
        
        if isLogged {
            self.performSegue(withIdentifier: "secondView", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Unwind Segue
    @IBAction func salirLogin(segue: UIStoryboardSegue){
        UserDefaults.standard.set(false, forKey: "isLogged")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        UserDefaults.standard.set(true, forKey: "isLogged")
    }
}
